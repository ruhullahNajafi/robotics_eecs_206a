#!/usr/bin/env python
import sys
import rospy
import moveit_commander
import string
from moveit_msgs.msg import OrientationConstraint, Constraints
from geometry_msgs.msg import PoseStamped
from intera_interface import gripper as robot_gripper


def main():
    #Initialize moveit_commander
    moveit_commander.roscpp_initialize(sys.argv)

    #Start a node
    rospy.init_node('moveit_node')

    #Initialize both arms
    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()
#    left_arm = moveit_commander.MoveGroupCommander('left_arm')
    right_arm = moveit_commander.MoveGroupCommander('right_arm')
#    left_arm.set_planner_id('RRTConnectkConfigDefault')
#    left_arm.set_planning_time(10)
    right_arm.set_planner_id('RRTConnectkConfigDefault')
    right_arm.set_planning_time(10)

    

    while (True):

        InputList = []
        InputList.append (raw_input("Please input X and hit <Enter>"))
        InputList.append (raw_input("Please input Y and hit <Enter>"))
        InputList.append (raw_input("Please input Z and hit <Enter>"))
        InputList = list(map(float,InputList))
        #First goal pose ------------------------------------------------------
        goal_1 = PoseStamped()
        goal_1.header.frame_id = "base"

        #x, y, and z position
        goal_1.pose.position.x = InputList[0]
        goal_1.pose.position.y = InputList[1]
        goal_1.pose.position.z = InputList[2]
        
        #Orientation as a quaternion
        goal_1.pose.orientation.x = 0.0
        goal_1.pose.orientation.y = -1.0
        goal_1.pose.orientation.z = 0.0
        goal_1.pose.orientation.w = 0.0

        #Set the goal state to the pose you just defined
        right_arm.set_pose_target(goal_1)

        #Set the start state for the right arm
        right_arm.set_start_state_to_current_state()

        #Create a path constraint for the arm
        #UNCOMMENT TO ENABLE ORIENTATION CONSTRAINTS
        orien_const = OrientationConstraint()
        orien_const.link_name = "right_gripper";
        orien_const.header.frame_id = "base";
        orien_const.orientation.y = -1.0;
        orien_const.absolute_x_axis_tolerance = 0.1;
        orien_const.absolute_y_axis_tolerance = 0.1;
        orien_const.absolute_z_axis_tolerance = 0.1;
        orien_const.weight = 1.0;
        consts = Constraints()
        consts.orientation_constraints = [orien_const]
        right_arm.set_path_constraints(consts)

        #Plan a path
        right_plan = right_arm.plan()

        right_gripper = robot_gripper.Gripper('right')
        print('Calibrating...')
        right_gripper.calibrate()
        rospy.sleep(2.0)
        #Execute the plan
        raw_input('Press <Enter> to move the right arm to goal pose: ')

        # OUR_CODE: 
        right_arm.execute(right_plan)
        rospy.sleep(2.0)
        #Open the right gripper
        raw_input('Press <Enter> to open gripper: ')
        print('Opening...')
        right_gripper.open()
        rospy.sleep(1.0)
        #Close the right gripper
        raw_input('Press <Enter> to close gripper: ')
        print('Closing...')
        right_gripper.close()
        rospy.sleep(1.0)
        print('Done!')

        again = raw_input('Do you want to do it again? (y or n)')
        if again == 'n':
            print('Why though? this program is soo cool')
            break


    

if __name__ == '__main__':
    main()

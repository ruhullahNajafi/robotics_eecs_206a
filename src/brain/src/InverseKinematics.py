################################################################################
#
# InverseKinematics class listens for LaserScans and builds an occupancy grid.
#
################################################################################

import rospy

from moveit_msgs.srv import GetPositionIK, GetPositionIKRequest, GetPositionIKResponse
from moveit_commander import MoveGroupCommander
from geometry_msgs.msg import PoseStamped

import numpy as np
from numpy import linalg

class InverseKinematics(object):
    def __init__(self):
        #ROS_INFO("Initializing InverseKinematics instance")
        #Wait for the IK service to become available
        rospy.wait_for_service('compute_ik') 
        #rospy.init_node('service_query')    # THIS DESTROYS EVERYTHING!!!!!!!!

        #Create the function used to call the service
        self.compute_ik = rospy.ServiceProxy('compute_ik', GetPositionIK)


    def move(self, goalPosition, goalOrientation):
        #ROS_INFO("InverseKinematics:move(%f,%f,%f)"%(goalPosition[0],goalPosition[1],goalPosition[2]))
        print("-> move()")
        request = GetPositionIKRequest()
        request.ik_request.group_name = "right_arm"
        request.ik_request.ik_link_name = "right_gripper"
        request.ik_request.attempts = 20
        request.ik_request.pose_stamped.header.frame_id = "base"
        
        #Set the desired orientation for the end effector HERE
        request.ik_request.pose_stamped.pose.position.x = goalPosition[0]
        request.ik_request.pose_stamped.pose.position.y = goalPosition[1]
        request.ik_request.pose_stamped.pose.position.z = goalPosition[2]
        request.ik_request.pose_stamped.pose.orientation.x = goalOrientation[0]
        request.ik_request.pose_stamped.pose.orientation.y = goalOrientation[1]
        request.ik_request.pose_stamped.pose.orientation.z = goalOrientation[2]
        request.ik_request.pose_stamped.pose.orientation.w = goalOrientation[3]
        
        try:
            #Send the request to the service
            response = self.compute_ik(request)
            
            #Print the response HERE
            print(response)
            group = MoveGroupCommander("right_arm")

            # Setting position and orientation target
            group.set_pose_target(request.ik_request.pose_stamped)

            # TRY THIS
            # Setting just the position without specifying the orientation
            ###group.set_position_target([0.5, 0.5, 0.0])

            # Plan IK and execute

            print("go()")
            print request.ik_request.pose_stamped.pose.position
            group.go()
            
        except rospy.ServiceException, e:
            print "InverseKinematics: Service call failed: %s"%e
        print("<- move()")


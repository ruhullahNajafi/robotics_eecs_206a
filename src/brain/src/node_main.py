#!/usr/bin/python
################################################################################
#
# Node to wrap the InverseKinematics class.
#
################################################################################

from InverseKinematics import InverseKinematics

import rospy
import sys
import numpy as np


if __name__ == "__main__":
	raw_input('Everything is Ready. Press ''Enter'' to go!!!')
	print("You pressed enter, lets start the movement of the robot!")
	rospy.init_node("node_main")
	print("starting node")
	ik = InverseKinematics()
	print("IK initialized")
	goalPosition = np.array([0.675, -0.566, 0.330])	# I copied [0.875, -0.566, 0.330] from the "rosrun tf tf_echo se right_wrist" command
	goalOrientation = np.array([-0.073, 0.897, 0.277, 0.337])
	ik.move(goalPosition, goalOrientation)

	# spin() simply keeps python from exiting until this node is stopped
	rospy.spin()

'''
class BuildHouse(object): 

	def __init__(self):
		print("Running. Ctrl-c to quit")

	def MainFunction(self):	
		print("MAIN RUNNING")


def main():
    # initialization
	Game = PlayGame()
    # run main function
	Game.MainFunction()
    

if __name__ == "__main__":
	main()
'''   	 

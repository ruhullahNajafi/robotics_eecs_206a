#!/bin/bash

# First of all make sure you have created a link to the baxter.sh shell script:
# ln -s /scratch/shared/baxter_ws/baxter.sh

# Connect to the robot:
# ./baxter.sh [robotname].local

echo "-> Starting awsome game ->"

echo "-> Start Camera setup done"
# Activate camera

echo "Closing all cameras...."
rosrun baxter_tools camera_control.py -c left_hand_camera
rosrun baxter_tools camera_control.py -c right_hand_camera
rosrun baxter_tools camera_control.py -c head_camera

echo "Turning on right hand camera..."
rosrun baxter_tools camera_control.py -o right_hand_camera
#rosrun baxter_tools camera_control.py -o left_hand_camera

echo "<- StartCamera setup done"

# Enable Baxter: 
rosrun baxter_tools enable_robot.py -e

echo "Running startBaxer.launch"
roslaunch brain startBaxter.launch

echo "Reached the End of the start.sh file"

# Our code:
#echo "Starting main.launch"
#roslaunch brain main.launch

#echo "<- Ending awsome game <-"

###############################################
# HOW TO SET UP THE SERVICES MANUALLY:


# rosrun baxter_tools enable_robot.py -e
# rosrun baxter_interface joint_trajectory_action_server.p
# roslaunch baxter_moveit_config move_group.launch
# roslaunch brain main.launch
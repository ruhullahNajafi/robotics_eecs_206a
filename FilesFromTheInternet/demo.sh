#!/bin/bash
roslaunch openni2_launch openni2.launch&
OPENNI2_PID=$!
kill_openni ()
{
	echo terminating openni
	kill $OPENNI2_PID
}
terminate ()
{
	kill_openni
	wait $OPENNI2_PID
	exit
}
trap terminate SIGINT SIGTERM
${SHELL} demo_baxter.sh&
BAXTER_SHELL_PID=$!
kill_baxter_shell ()
{
	echo terminating baxter shell;
	kill $BAXTER_SHELL_PID;
}
terminate ()
{
	kill_openni
	kill_baxter_shell
	wait $OPENNI2_PID
	wait $BAXTER_SHELL_PID
	exit
}
trap terminate SIGINT SIGTERM

# Wait until the script receives SIGINT / SIGTERM
while :
do
        sleep 60
done
